# Sotva se narodíš
## Hop trop

1. [: `C`Sotva se `G7`narodíš, `C`už ti koně `G7`kovou, :]
[: `F`šavli ti `C`chystají `G`ocelo`C`vou :]
`G`ocelo`Am`vou, `Dm`bez pará`G7`dy,
[: `F`co kmáni `C`dostávaj' `G7`u armá`C`dy. :]

1. [: Sotva se narodíš, už si verbíř píše, :]
[: že nejsi ze zámku, ale z chýše, :]
ale z chýše pod horama,
[: těžko se vyplácet šesťákama. :]

1. [: Sotva se narodíš, už ti kulku lijou, :]
[: kdo střelí dřív, toho nezabijou, :]
nezabijou, a pak možná,
[: jakej maj' mrzáci život, pozná. :]

1. [: Sotva se narodíš, už ti šijou kabát, :]
[: kterej si voblíkneš jednou, dvakrát, :]
jednou, dvakrát, naposledy,
[: zelený sukno je zkrvavený. :]

1. [: `D`Kdejakej `A7`generál, `D`kdejakej `A7`kaprál, :]
[: `G`s flintama `D`lidi by `A`do pole `D`hnal, :]
`A`do pole `Hm`hnal `Em`proti `A7`sobě,
`G`komu jsou `D`metály `A7`platný v hro`D`bě?
`G`komu jsou `D`metály `A7`platný v hro`Hm`bě?
