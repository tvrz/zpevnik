# Divoké koně
## Jarek Nohavica


1. [: `Am`Já viděl `Dm`divoké `Am`koně, `C`běželi `Dm`soumra`Am`kem. :]
`Dm`Vzduch `Am`těžký `Dm`byl a divně `Am`voněl `G#dim F`tabákem,
`Dm`vzduch `Am`těžký `Dm`byl a divně `Am`voněl `E7`tabá`Am`kem.

2. [: Běželi, běžěli, bez uzdy a sedla, krajinou řek a hor. :]
[: Sper to čert, jaká touha je to vedla za obzor. :]

3. [: Snad vesmír nad vesmírem, snad lístek na věčnost. :]
[: Naše touho ještě neumírej, sil máme dost. :]

4. [: V nozdrách sládne zápach klisen na břehu jezera. :]
[: Milování je divoká píseň večera. :]

5. [: Stébla trávy sklání hlavu , staví se do šiku. :]
[: Král s dvořany přijíždí na popravu zbojníků. :]

6. [: chtěl bych jak divoký kůň běžet běžet, nemyslet na návrat. :]
[: S koňskými handlíři vyrazit dveře, to bych rád. :]

7. Já viděl divoké koně…

&nbsp;
![kone](ilustrace/kone.jpg "center")
