# Hej, člověče Boží
## Nezmaři

1. `Am`Hej, člo`Em`věče Bo`Am `ží, `C`zahodil `G`jsi bo`C`ty,
`Am`jakpak bez `Em`nich půjdeš `Am`dál, `F`touhletou `G`dobou sně`Am`ží,
`C`nehřeje `G`tě slun`C`ce, `Am`mám o `Em`tebe `Am`strach.

1. Hej, člověče Boží, zahodil jsi kabát,
jakpak bez něj půjdeš dál, pár dní před Vánoci.
Nehřeje tě slunce, mám o tebe strach.

1. Hej, člověče Boží, zahodils' peníze,
jakpak bez nich půjdeš dál, nekoupíš si chleba.
Nedají ti najíst, mám o tebe strach.

1. Hej, člověče Boží, zahodil jsi dřevo,
jakpak chceš v tý zimě spát, čas je o Vánocích.
Světnici máš prázdnou, mám o tebe strach.

1. Hej, člověče Boží, koho si to vedeš,
dívka zatoulaná, bez halíře v kapse,
cizí dítě porodí, mám o tebe strach.

1. Hej, člověče Boží, zahodil jsi boty,
jakpak bez nich půjdeš dál, touhletou dobou sněží.
Nehřeje tě slunce, mám o tebe strach.

1. `F`Hej, člo`G`věče Bo`A`ží…
