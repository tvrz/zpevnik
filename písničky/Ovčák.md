# Ovčák
## Spirituál kvintet

1. Na ov`D`čáka `A`kaprál `D`mává: „Dej se k nám a `A`nemine tě `D`sláva!“
Ovčák váhá, `A`nemá `D`stání, rozhlíží se `A`naposledy `D`strání.
Nechal ovce v louce `A`stát. Nesmě`D`le, nesmě`A`le
na po`D`chod začal `A`vojsku bubno`D`vat.

2. Na pochod se vojsko dává. Kaprál volá: „Králi budiž sláva!“
Vpředu tambor s bubnem víří, do bitevní vřavy rovnou míří.
Trubka vojenská ať zní, kanón zní, kanón zní,
vyhrává marš na cestu poslední.

3. Na šavlích se slunce blejská, po ovcích se ovčákovi stejská.
Po zahrádce s planou růží. Není buben, má jen jednu kůži.
Na vojáka proč si hrám? Buben sem, buben tam.
K čertu s ním může táhnout i pan král.

4. V dálce za ním bitva hřímá. Buben pryč a paličky jen třímá.
Cestou stokrát známou běží, nevadí, že popadá dech stěží.
Sláva už ho neláká. Ovečkám, ovečkám
bude dál v klidu dělat ovčáka (čtveráka).