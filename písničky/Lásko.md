# Lásko
## Karel Kryl

1. `Am`Pár zbytků pro krysy na misce od guláše,
`E`milostný dopisy s partií  `Dm E`mariáše,
`Dm`před cestou dalekou zpocený boty zujem
`C`a potom pod dekou `E`sníme, když onanujem.

> `Am`Lásko, `G`zavři se do pokoje,
`Am`lásko, `G`válka je holka moje,
`C`s ní se `G`milu`Am`ji, když `G`noci si `Am`krátím,`E`
`Am`lásko, `G`slunce máš na vějíři,
`Am`lásko, `G`dvě třešně na talíři,
`C`ty ti `G`daru`Am`ji, až `G`jednou se `Am`vrátím.`E`

2. Dvacet let necelých, odznáček na baretu,
s úsměvem dospělých vytáhnem cigaretu,
v opasku u boku nabitou parabelu,
zpíváme do kroku pár metrů od bordelu.

> Lásko…

&nbsp;
&nbsp;
&nbsp;
&nbsp;

3. Pár zbytků pro krysy a taška na patrony,
latrína s nápisy, jež nejsou pro matróny,
není čas na spaní, smrtka nám drtí palce,
nežli se zchlastaní svalíme na kavalce.

> Lásko…

4. Rec: `E`Levá, dva!

> Lásko…

&nbsp;
