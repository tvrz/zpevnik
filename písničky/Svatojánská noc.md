# Svatojánská noc
## Mezitím

1. `Em`Zněly zvony krajem `C`z dálky, `G`večerem vítr `D`vál,
`Em`voják co se vracel `C`z války `G`u jedný z chalup zakle`D`pal,
že `Am`nemá kde by v noci hlavu `Em`složil
a `Am`zlatou mincí platí za svůj `H7`dík
[: a jak `G`do dveří vchází, `D`netuší, života `C`zbývá jenom `Em`mžik. :]

2. Starý muž a stará paní poslouchají příběh vojákův,
kde bojoval a kde se zranil a že má váček plný zlaťáků,
ten váček zlata s sebou nese domů
pro starou mámu jako velkej dík
[: a jak vypráví voják netuší, života zbývá jenom mžik. :]

> Je `G`Svatýho Jána a `D`s půlnocí prý `C`otvírá se `Em`zem,
je `G`Svatýho Jána, noc `D`pokladů a `C`kouzel divých `Em`žen.
V tý `G`noci se `D`vzbouzí `Em`zloba a `D`pýcha,
osud `Em`divný `D`karty `Em`míchá.
Je `G`Svatýho Jána a `D`k půlnoci už `C`zbývá chvíle `Em`jen.

3. Mince se ve světle blýská, když starý v krbu přiloží,
ten váček zlata musí získat, jen co ke spánku se uloží,
pak ostrou dýku do srdce mu vnoří,
vždyť nikdo přece neví, že tu byl
[: a jak usíná voják netuší, života zbývá jen pár chvil. :]

> Je Svatýho Jána…

4. Na věži už půlnoc troubí, je z lesů slyšet vlčí zpěv.
Dvě postavy tam jámu hloubí, na rukou mají čerstvou krev,
lopotí se svatojánskou nocí, je hotovo,
když slunce vychází
[: a jak ráno se končí ta hrůzná noc, k domu soused přichází. :]

5. Hej sousede, dobré ráno, povídají lidé někteří,
že dědinou šel včera voják a zamířil k vám do dveří, říkal
tady jeho cesta provždy končí, že je doma, že se vrací k svým,
[: že vás překvapí zářil radostí, z války vrátil se vám syn. :]
