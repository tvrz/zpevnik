# Batalion
## Spirituál kvintet

> `Am`Víno `C`máš a `G`marky`Am`tánku, `C`dlouhá noc `G`se `Am`pro`Em`hý`Am`ří,
víno `C`máš a `G`chvilku `Am`spánku, `C`díky, dí`G`ky, `Am`ver`Em`bí`Am`ři.

1. `Am`Dříve než se rozední, kapitán `C`k osedlání `G`rozkaz `Am`dá`Em`vá,
`Am`ostruhami do slabin ko`G`ně `Am`po`Em`há`Am`ní.
`Am`Tam na straně polední, čekají `C`ženy, zlaťá`G`ky a `Am`slá`Em`va,
`Am`do výstřelů z karabin zvon `G`už `Am`vyz`Em`vá`Am`ní.

>> `Am`Víno na ku`C`ráž a `G`pomilovat marky`Am`tánku,
`C`zítra do Burgund Bata`G`lion `Am`za`Em`mí`Am`ří.
Víno na ku`C`ráž a `G`k ránu dvě hodinky `Am`spánku,
`C`díky, díky Vám králov`G`ští `Am`ver`Em`bí`Am`ři.

2. Rozprášen je Batalion, poslední vojáci se k zemi hroutí,
na polštáři z kopretin budou věčně spát.
Neplač sladká Marion, verbíři nové chlapce přivedou ti,
za královský hermelín padne každý rád.

>> Víno na kuráž…

> Víno máš…
