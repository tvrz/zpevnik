# Velká Jizera
## Marcel Šťepánek - Fox
1. `Am`Klikatá ces`C`tička `F`jde na Buko`C`vec,
`F`pročpak tudy `C`chodil, pročpak tudy `Am`chodil,
švarný mláde`E`nec.
Bylo to o pouti dole v Hejnicích,
kde si zamiloval, kde si zamiloval
Kuba její smích.

> `Am`Temná `G`Jizera, `C`temná `G`Jizera `C`valí `G`svý vody `C`v dál,
`Em`pod Smrkem pramení, `Am`tajuplně šumí `F`na písči`E`nách.
Velká Jizera, Velká Jizera nechoď tam v noci sám,
za svitu měsíce, kouzel na tisíce, nevrátíš se k nám.

2. Koho sis namluvil, Kubo bláhovej,
její táta Tapper, její táta Tapper,
pytlák velikej.
Za noci vodlejvá kule ďábelský,
potom bez míření, potom bez míření,
trefí každej cíl.

> Temná Jizera …

3. Do noci prchají Kuba s dívčinou,
hlubokými lesy, hlubokými lesy,
musí proběhnout.
Mysleme my na ně, ať ven vyběhnou,
nežli starej Tapper, nežli starej Tapper,
střelí kuli zlou.

> Temná Jizera …