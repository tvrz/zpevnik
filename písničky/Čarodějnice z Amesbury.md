
# Čarodějince z Amesbury
## Asonance


1. Zuzana `Dm`byla dívka, `C`která žila `Dm`v Amesbury,
s jasnýma `F`očima a `C`řečmi pánům `Dm`navzdory,
sousedé `F`o ní říka`C`li, že `Dm`temná kouzla `Am`zná
a `B`že se lidem `Am`vyhýbá a `B`s ďáblem `C`pletky `Dm`má.

1. Onoho léta náhle mor dobytek zachvátil
a pověrčivý lid se na pastora obrátil,
že znají tu moc nečistou, jež krávy zabíjí,
a odkud ta moc vychází, to každý dobře ví.

1. Tak Zuzanu hned před tribunál předvést nechali,
a když ji vedli městem, všichni kolem volali:
„Už konec je s tvým řáděním, už nám neuškodíš,
teď na své cestě poslední do pekla poletíš!“

1. Dosvědčil jeden sedlák, že zná její umění,
ďábelským kouzlem prý se v netopýra promění
a v noci nad krajinou létává pod černou oblohou,
sedlákům krávy zabíjí tou mocí čarovnou.

1. Jiný zas na kříž přísahal, že její kouzla zná,
v noci se v černou kočku mění dívka líbezná,
je třeba jednou provždy ukončit ďábelské řádění
a všichni křičeli jak posedlí: „Na šibenici s ní!“

1. Spektrální důkazy pečlivě byly zváženy,
pak z tribunálu povstal starý soudce vážený:
„Je přece v knize psáno: nenecháš čarodějnici žít
a před ďáblovým učením budeš se na pozoru mít!“

1. Zuzana stála krásná s hlavou hrdě vztyčenou
a její slova zněla klenbou s tichou ozvěnou:
„Pohrdám vámi, neznáte nic nežli samou lež a klam,
pro tvrdost vašich srdcí jen, jen pro ni umírám!“

1. Tak vzali Zuzanu na kopec pod šibenici
a všude kolem ní se sběhly davy běsnící,
a ona stála bezbranná, však s hlavou vztyčenou,
zemřela tiše samotná pod letní oblo`G`hou.
