# Krajina slepců
## Klíč

1. Hm hm hm…

2. `Em`Půlnočním krajem vzhůru had se drápe,
`G`slepci jdou, slepý vůdce v čele tápe,
`Am`přísloví se může skutkem stát,
nejdřív `Em`krásný `D`sen, a pak `Em`pád.

3. Tam za vodou, tam rostou jitrnice na stromech
a sýr na polích a sudy vína v záhonech,
tak jásej, hraj, křepči ve tmě jako pán,
už ráno dojdem tam, kde je ráj.

> `Am`Zástupy jdou, slepců `Em`najde se vždy dost
a `G`dav chce své `D`vidiny `Em`mít,
`Am`pojď, zvedni plášť, a než `Em`zlámeš vaz i kost,
můžeš `G`chvíli jak `D`světa pán `Em`žít.

4. Půlnočním krajem vzhůru had se drápe,
slepci jdou, slepý vůdce v čele tápe,
přísloví se může skutkem stát,
nejdřív krásný sen, a pak pád.

5. Noc, keřů pár, skály a stezka vede kolmo vzhůru,
důvěru měj, tvůj vůdce, předák, pán, guru
sám nejlíp ví, kde jsou buchty ve větvích
pak jehňat výkřik a zlý noci smích.

> Zástupy jdou…
