# Tři bratři
## Spirituál kvintet

1. `Am`Tři bratři žili kdys v zemi skotské,
v domě zchudlém jim `D/A`souzeno `Am E`žít,
ti `Am`kostkama metali, kdo z nich má jíti,
`D/A`kdo z nich má `Am Em`jít,
`F`kdo z nich má `C`na moři `G`pirátem `Am`být.

2. Los padl a Henry už opouští dům,
ač je nejmladší z nich, vybrán byl, ![lod](ilustrace/lod.jpg "right")
by koráby přepadal, na moři žil,
na moři žil,
své bratry z nouze tak vysvobodil.

3. Po dobu tak dlouhou, jak v zimě je noc,
a tak krátkou, jak zimní je den,
už plaví se Henry, když před sebou objeví
loď, pyšnou loď:
Napněte plachty a kanóny ven!

3. Čím kratší byl boj, tím byl bohatší lup,
z vln už ční jenom zvrácený kýl,
teď Henry je boháč, když boháče oloupil,
loď potopil,
své bratry z nouze tak vysvobodil.

4. Do Anglie staré dnes smutná jde zvěst,
smutnou zprávu dnes dostane král,
ke dnu klesla pyšná loď poklady Henry si
vzal, on si vzal;
střezte se moře, on vládne tam dál!
