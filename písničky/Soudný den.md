# Soudný den
## Spirituál kvintet

1. `Am`Zdál se mi sen, že se nebe hroutí,
`G`zdál se mi sen o poslední pouti,
`Am`zdál se mi sen, že všechno seberou ti, v ten `Em7`soudný `Am`den.

2. Kam běžet mám, Slunce rychle chladne,
kam běžet mám, měsíc na zem spadne,
kam běžet mám, moře už je na dně, v ten soudný den.

3. Stůj, nechoď dál, času už je málo,
stůj, nechoď dál, míň, než by se zdálo,
stůj, nechoď dál, otevři se, skálo, v ten soudný den.

4. Pán tě zavolá, má pro každého místo,
Pán tě zavolá, jen kdo má duši čistou,
Pán tě zavolá, sám nedokázal bys to, v ten soudný den.

> `Am`Soudí, soudí pány, slouhy, `G`soudí, soudí hříšné touhy,
`Am`soudí, soudí, výčet pouhý, `Am Esus4 E`

5. `Am`V tom se probudíš, to byl jen sen,
`F`v tom se probudíš, to byl jen sen,
`Dm`v tom se probudíš, to byl jen sen, `Am`jen `E`pouhý `Am`sen.

6. Zdál se mi sen, že se nebe hroutí,
zdál se mi sen o poslední pouti,
zdál se mi sen, že všechno seberou ti, v ten soudný den.

7. Zdál se mi sen, já stojím na svém místě,
zdál se mi sen, mé svědomí je čisté,
`Am`zdál se mi sen, jen jedno vím `F`jistě: `Am`je `E`soudný `Am`den!
