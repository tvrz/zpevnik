# Až se k nám právo vrátí
## Spirituál kvintet

1. Chci sl`Dm`uncem být a ne planetou,
`A7`až se k nám právo vr`Dm`átí,
chci setřást bázeň staletou,
`A7`až se k nám právo vr`Dm`átí.

> `Dm`Já čekám dál, já `C`čekám `F`dál,
`B`já čekám `A`dál,`A7` až se k nám právo `Dm`vrátí, v``Gm``rátí,``A``
`Dm`já čekám dál, já `C`čekám `F`dál,
`B`já čekám `A`dál,`A7` až se k nám právo `Dm`vrátí.

2. Kam chci, tam půjdu, a co chci, budu číst,
až se k nám právo vrátí,
a na co mám chuť, to budu jíst,
až se k nám právo vrátí.

3. Já nechci už kývat, chci svůj názor mít,
až se k nám právo vrátí,
a jako člověk chci svůj život žít,
až se k nám právo vrátí.

> Já čekám dál…

5. Chci klidně chodit spát a beze strachu vstávat,
až se k nám právo vrátí,
své děti po svém vychovávat,
až se k nám právo vrátí.

6. Už se těším až se narovnám,
až se k nám právo vrátí,
své věci rozhodnu si sám,
až se k nám právo vrátí. ![ruze](ilustrace/ruze.jpg "right")

> Já čekám dál…

&nbsp;
