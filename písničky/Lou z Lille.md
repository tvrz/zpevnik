# Lou z Lille
## Klíč

1. Ona `C`jmenuje se `F`Lou a `C`pochází prý `G`z Lille,
má `C`čertů rohy, `F`křídla andělů, `C`půvab `G`lesních `C`víl.

2. Kdo strávil s ní pár chvil, jak šampaňské by pil,
má úsměv tupců, trubců závratě pro ni, pro Lou z Lille.

> [: Jako `C`srpky luny boky `F`tenké má a `C`úsměv velkých `G`dam,
tak se `C`lehce `F`vznáší `C`nad ze`G`mí, letí, `C`vůbec `G`netuší, `C`kam. :]

3. Dětsky vážný hlas, větrem urousaný vlas
a oči jako okna za plotem černočerných řas.

4. Co `D`já vím, nemá `G`dům, ale `D`asi ani `A`byt,
a `D`přesto každý `G`kluk chce náramně `D`tam, kde `A`ona, `D`být.

>> [: Jako `D`srpky luny boky `G`tenké má a `D`úsměv velkých `A`dam,
tak se `D`lehce `G`vznáší `D`nad ze`A`mí, letí, `D`vůbec `A`netuší, `D`kam. :]

5. `C`Víno, vejce, `F`sýr, taky `C`čerstvých ryb dost `G`má,
`C`tohle do košíku `F`každý na trhu `C`zadar`G`mo jí `C`dá.

6. A báby závidí, mají každé ráno zlost,
a my si ji tu pěkně hýčkáme jen tak, pro radost.

> Jako srpky luny…

7. Ona `D`jmenuje se `G`Lou a `D`pochází prý `A`z Lille,
má `D`čertů rohy, `G`křídla andělů, `D`půvab `A`lesních `D`víl.
