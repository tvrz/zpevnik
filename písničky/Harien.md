# Harien
## Mezitím

1. Město `Dm`v plamenech, bitva `C`doznívá,
v domech `Am`drancování, mučení a `Dm`krev a křik.
Jásot `F`vítězů, král se `C`usmívá,
dneska `Am`v noci chce mít pannu čistou `Dm`jako sníh.

> Harien, Hari`F`en, Hari`C`en, tělo `Am`bílý jako sníh, Hari`Dm`en.

2. Jsi jen kořistí pouhou válečnou,
pro potěšení krále, jedna z mnoha žen.
Král už přichází, v očích chtíč,
však se svlíkej princezno Harien.

3. Tělo čistý máš, slzy na víčkách,
jako perly který padají na chladnou zem,
s krutým úsměvem král se dívá,
jak se svlíkáš malá Harien.

> Harien…

4. Kolik `F`dívčích snů teď v tobě `C`umírá,
srdce `Am`rozbitý na střípky `Dm`posbírá,
něco skříplo dávnejm `C`hodinkám,
kde teď `Am`jsou tajemství šeptaný `Dm`panenkám,
tak se `F`v duchu vracíš do dětství `C`k mamince,

když jsi `Am`před usnutím čekávala `Dm`na prince,
a namísto prince `C`démon jen, `Am`až to přijde, ![dyka](ilustrace/dyka.jpg "right")
zavři oči `Dm`Harien.

5. Nahá před králem, jenom za zády
svíráš chladnou čepel ostré dýky ve dlaních,
už se blíží, ruku vztáh,
kdepak zůstal Bůh a kdo tě zachrání.

> Harien…

6. Už se tě dotýká, radši mrtvá být,
než vydat svoje čistý tělo králi v plen,
a pak ve svíčkách ostří se zablýskne,
kde je srdce krále míří dýka Harien.

> Harien, rukou tvojí zemřel král, Harien.
