# Za Boha, za žold a za víru
## Lucrezia Borgia

1. `Dm`Bitvu hned oslavíme `F`před bojem, `C`z kalichu vína `Dm`popijem,
`Dm`frátři ať požehnaj `F`čepele, `C`ať můžem umřít `Dm`vesele,
ať `C`umřem vese`Dm`le.

Děvčata, kterou z vás neláká potěšit před bojem žoldáka,
nač je mu nadité tobolky, když chuť by neměl na holky,
už chuť i na holky.

Hejá hop…

2. Hej hola, padl už první val, přední voj těly ho zasypal,
s pravdou, co vítězí nad falší, hrabem se zbytečně na další,
se hrabem na další.

Za boha, za žold a za víru ať ve Flandrech nebo v Alžíru,
ať pod Karlem nebo s Filipem, jen další val těly zasypem,
další val zasypem.

Hejá hop…
