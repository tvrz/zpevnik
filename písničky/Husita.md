
# Husita
## Jarek Nohavica

1. [: `C`Pásával jsem koně `G`u nás ve dvoře `C`ale už je nepa`G`su. :]
`Am`Chudák ten je `G`dole a `C`pán naho`F`ře, `C`všeho jenom `G`doča`Am`su,
`F`jó `C`všeho jenom `G`doča`C`su.

2. [: Máma ušila mi režnou kytlici padla mi jak ulitá. :]
Táta vytáh ze stodoly sudlici a teď seš chlapče husita,
jó a teď seš chlapče husita.

> `C`Hejtman volá: `G`Do zbra`C`ně! `Am`Bijte `Dm`pány `G7`hrr na `C G7`ně!
`C`Hejtman volá: `G`Do zbra`C`ně! `Am`Bijte `Dm`pány `G7`hrr na `C`ně!
`Am`A mně `Dm`srdce `G7`buší `C`lásce dal jsem `Am`duši `Dm`jen ať s námi `G`zůsta`C`ne.

3. [: U města Tachova stojí křižáci leskne se jim brnění. :]
Sudlice je těžká já se potácím dvakrát dobře mi není,
jó dvakrát dobře mi není.

4. [: Tolik hezkejch holek chodí po světě já žádnou neměl pro sebe. :]
Tak si říkám: Chlapče křižák bodne tě a čistej půjdeš do nebe,
jó čistej půjdeš do nebe.

> Hejtman volá…

5. [: Na vozové hradbě stojí Marie mává na mě zdaleka. :]
Křižáci kdo na ni sáhne mordyjé ten se pomsty dočeká,
jó ten se pomsty dočeká.

6. [: Chtěl jsem jí dát pusu tam co je ten keř řekla: To se nedělá! :]
Když mě nezabijou to mi holka věř budeš moje docela,
jó budeš moje docela.

> Hejtman volá…

7. [: Už se na nás valí křižáci smělí zlaté kříže na krku. :]
Jen co uslyšeli jak jsme zapěli zpět se ženou v úprku,
jó zpět se ženou v úprku.

8. [: V trávě leží klobouk, čípak mohl být?
&nbsp;   Prý kardinála z Anglie. :]
Tam v té trávě zítra budeme se mít já a moje Marie,
jé ať miluje kdo žije,
jé ať žije historie.
