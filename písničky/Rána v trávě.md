# Rána v trávě
## Žalman

> `Am`Každý ráno boty `G`zouval, `Am`orosil si nohy v `G`trávě,
`Am`že se lidi mají rádi `G`doufal `Am`a pro`Em`citli `Am`právě.
`Am`Každy ráno dlouze `G`zíval, `Am`utřel čelo do ru`G`kávu,
`Am`a při chůzi tělem semtam `G`kýval, `Am`před se`Em`bou sta `Am`sáhů.

1. `C`  Poznal `G`Mora`F`věnku `C`krásnou `Am`  a ` G`vínečko `C`ze zlata,
v Čechách `G`slávu `F`muzi`C`kantů `Am`  uma`Em`zanou `Am`od bláta.

> Každý ráno…

2. Toužil najít studánečku a do ní se podívat,
by mu řekla: proč, holečku, musíš světem chodívat.

3. Studánečka promluvila: to ses' musel nachodit,
abych já ti pravdu řekla, měl ses' jindy narodit.

> Každý ráno…
[: před sebou sta sáhů :]
