# Bůh v nás
## Ginevra

1. `Am`Za obzorem zní `C`píseň havra`G`ní, ohně pohasnou, slyš Pane `Am`náš.
`Em`Zas `Am`přijde nový den, `C`krví potřís`G`něn, Bože sílu máš, snad přeži`Am`jem.

> `Em`Dnes, `Am`ó Pane náš, amen požeh`C`nej.
Víru `G`v nás zachovej, pro ni jdem se `F`bít.
`Em`Vždyť `Am`víš, Pane náš, smrt není na pro`C`dej,
modlit`G`bou Otčenáš ať přisouzeno `F`nám `Em`je `Am`žít.

2. Zbraně pozvednem, k boji potáhnem v srdcích víru svou, ó Pane náš.
Ty křížem ochráníš duše bratří tvých, Bože, sílu máš, proč umírat.

> Dnes, ó Pane náš…

3. Slunce uvadá, v dýmu zapadá, tma uléhá k nám, ó Pane náš.
Tvé oči uvidí světlo dalsích dní, Bože, sílu máš, poutem jsi v nás.

> Dnes, ó Pane náš…
