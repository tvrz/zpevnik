# Pohár a kalich
## Klíč

1. `Em`Zvedněte poháry, `D`pánové, už jen `G`poslední `D`přípitek `Em`zbývá,
pohleďte, nad polem `D`Špitálským `G`vrcholek `D`roubení `G`skrývá,
[: za chvíli `D`zbyde tam popel a `Em`tráva,
nás čeká `D`vítězství, bohatství, `Em`sláva. :]

2. Ryšavý panovník jen se smál, sruby prý dobře se boří,
palcem k zemi ukázal, ať další hranice hoří, ![kalich](ilustrace/kalich.jpg "right")
[: ta malá země už nemá co získat,
teď bude tancovat, jak budem pískat. :]

> `Am`Náhle se pozvedl `D`vítr a mocně `G`vál,
`Am`odněkud přinesl `D`nápěv, sám si ho `H7`hrál:
`Am G Em H7 Em`hm…
`Em`Zvedněte poháry, `D`pánové, večer `G`z kalichu `D`budeme `Em`pít.

3. Nad sruby korouhev zavlála, to však neznačí, že je tam sláva,
všem věrným pokaždé nesmírnou sílu a jednotu dává,
[: ve znaku kalich, v kalichu víra,
jen pravda vítězí, v pravdě je síla. :]

4. „Modli se, pracuj a poslouchej,“ kázali po celá léta,
Mistr Jan cestu ukázal proti všem úkladům světa,
[: být rovný s rovnými, muž jako žena,
dávat všem věcem ta pravá jména. :]

5. Do ticha zazněla přísaha -- ani krok z tohoto místa,
se zbraní každý vyčkává, dobře ví, co se chystá,
[: nad nimi stojí muž, přes oko páska,
slyšet je dunění a dřevo praská. :]

> Náhle se pozvedl vítr a mocně vál,
odněkud přinesl nápěv, sám si ho hrál:
`Am`Kdož jsú boží bo`G`jovníci `Em`a zákona `H7`je`Em`ho,
`C`prostež od Bo`Am`ha pomoci a úfajte v ně`G`ho.
