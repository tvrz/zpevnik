# Dudy z Edinburku
## Spirituál kvintet

1. Sotva `F`začnou v Edinburku dudy hrát,
celé město vyrazí ven `C`tancovat,
Skoti, `F`Skotky oblečou si suknice,
potom dupou, až se tresou `C`uli`F`ce.

> Derry `F`don don duli duli du dá dej,
pěkně dupni potom řadu `C`vyrovnej,
derry `F`don don duli duli du dá dej,
tenhle tanec mají Skoti `C`nejra`F`děj.

Čtyři `F`kroky tam, duli duli dam,
čtyři `B`kroky sem duli duli dam
`F`kdo na tyhle čtyři kroky nesta`C`čí,
ten si `F`se Skoty a se Skotkami neza`C`skota`F`čí.

2. Skotská kostka červená a zelená,
skotské třapce ozdobí nám kolena
každou chvíli holka letí povětřím,
Skoti pohybem totiž nešetří.

> Derry don don…

3. Skotskou whisku naleju si do sebe,
skotskou sukni roztočím až do nebe
pak dám skotskou pusu na tvou skotskou tvář,
neboť pravý Skot je přece sukničkář. ![dudy](ilustrace/dudy.jpg "right")

> Derry don don…

&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
