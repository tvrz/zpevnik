# O malém rytíři
## Traband

> `Am`Jede jede rytíř, `C`jede do kraje,
`G`nové dobrodružství `Am`v dálce hledaje,
`Am`neví co je bázeň, `C`neví co je strach,
`G`má jen velké srdce `Am`a na botách prach.

1. `F`Jednou takhle v neděli, `E`slunce pěkně hřálo,
`F`bylo kolem poledne, `E`když tu se to stalo,
`F`panáček uhodí `G`pěstičkou do stolu:
`F`„Dosti bylo pohodlí `G`a plnejch kastrólů!
`Am`Ještě dneska stůj co stůj `G`musím na cestu se dát,
`F`tak zavolejte sloužící a `E`dejte koně osedlat!“

> Jede jede rytíř…

2. „Ale milostpane!“ spráskne ruce starý čeledín,
ale pán už sedí v sedle a volá s nadšením:
„Má povinnost mi velí pomáhat potřebným,
ochraňovat chudé, slabé, léčit rány nemocným.“
Marně za ním volá stará hospodyně:
„Vraťte se pane, lidi jsou svině!“

> Jede jede rytíř…

3. Ale sotva dojel kousek za městskou bránu,
z lesa na něj vyskočila banda trhanů,
všichni ti chudí, slabí, potřební, ta chátra špinavá,
vrhli se na něj a bili ho hlava nehlava.
Než se stačil vzpamatovat, bylo málem po něm,
ukradli mu co kde měl a sežrali mu koně.

> Jede jede rytíř…

4. „Vzhůru srdce!“ zvolá rytíř, nekončí má pouť,
svou čest a slávu dobudu, jen z cesty neuhnout!
Hle, můj meč! (a zvedl ze země kus drátu)
a zde můj štít a přílbice! (plechovka od špenátu)
Pak osedlal si pavouka, sedl na něj, řekl „Hyjé!
Jedem vysvobodit princeznu z letargie.“

> Jede jede rytíř…

5. A šíleně smutná princezna sotva ho viděla,
vyprskla smíchy a plácla se do čela,
začala se chechtat až jí z očí tekly slzy,
„To je neskutečný,“ volala, „jak jsou dneska lidi drzý!
O mou ruku se chce ucházet tahle figůra,
hej, zbrojnoši, ukažte mu rychle cestu ze dvora!“

> Jede jede rytíř…

6. Tak jede malý rytíř svojí cestou dál,
hlavu hrdě vzhůru - on svou bitvu neprohrál,
i když král ho nechal vypráskat a drak mu sežral boty
a děvka z ulice mu plivla na kalhoty.
Ve světě kde jsou lidi na lidi jak vlci,
zůstává rytířem - ve svém srdci.

> Jede jede rytíř…
