# Balada o věčné lásce
## Klíč

1. Byl jest `Am`sličný `G`rytíř `Am`z cínu, či snad lacknecht, `G`muške`Am`týr,
na poličce `G`v polo`Am`stínu, barevný měl `G`štít i `Am`knír,
nad hla`G`vou meč nehý`C`bá se, není `E`slyšet bitvy `Am`řev,
tak tu `G`stojí v plné `C`kráse, chce být `E`tvůj a chce prolít `Am G Am G Am`krev.
A co `Dm`růže, `C`která `Dm`vadne v čínské `F`váze `C`opo`Dm`dál,
za ok`F`ny už `C`podzim `Dm`chřadne, ona natruc `C`voní `Dm`dál,
kolik `C`plátků ještě `F`zbývá, zrcad`A`lo má matný `Dm`jas,
na se`C`be se růže `F`dívá, chce být `A`tvá a chce přelstít `Dm C Dm C Dm`čas.

2. A ta růže, co se tváří, že se jí čas netýká,
přes den pyšně vůkol září, v noci tajně naříká,
okamžik se nedá vrátit, marně čekáš na zázrak,
nechceš-li mou lásku ztratit, neříkej ještě ne, až pak.
A ten rytíř, co se věčně chystá ztéci lásky věž,
na poličce nekonečně vymlouvá se, je to lež,
ten, kdo váhá, nese vinu, buďto útok, nebo krach,
ty jsi jak ten rytíř z cínu, cit je cit, ale strach je strach.
   
3. A tak v prvním chvatu mládí klečíš lásce u nohou,
koho kdo tu vlastně svádí, slova nic už nezmohou,
jí je mdlo a dech se tají, a ty rudneš jako rak,
oba chcete být už v ráji, peklem tam cesta vede však.
Končí téměř bezúhonně tahle věčná balada,
končí pro nás, ne však pro ně, to vás jistě napadá,
byl jest jeden sličný, inu, to už znáte, a tak dál,
a ten v něžném polostínu dívce prý růži na klín dal.