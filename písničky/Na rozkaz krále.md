# Na rozkaz krále
## Asonance

1. Mám se `Am`bít na rozkaz `C G`krále, život `F`svůj mám za něj `C G`dát
a žít `Am`v nejistotě `C G`stálé, v řadách `F`vojska pevně stát,
v řadách `C`anglické armá`G`dy mám si `F`na vojáka `C G`hrát,
v téhle `Am`nesmyslné `C G`válce jedním `F`z hrdinů se `Am`stát.
   
2. Skotské kopce jsou mou láskou, mým jediným královstvím,
proč ti musím sbohem dávat, proč to všechno opouštím,
mám-li bojovat za lásku, za své děti a svůj dům,
pak i život svůj dám v sázku, ne však za anglický trůn.
   
3. Z dálky rachot bubnů sílí, naše loučení je blíž,
i když říkám, že se vrátím, nevrátím se, ty to víš,
v poli stojí řada křížů, i ten svůj tam vidím stát,
ze tvých očí slzy stírám, neplač, lásko, mám tě rád.

4. Mám se bít na rozkaz krále, život svůj mám za něj dát
a žít v nejistotě stálé, v řadách vojska pevně stát,
v řadách anglické armády mám si na vojáka hrát,
v téhle nesmyslné válce jedním z hrdinů se stát.