# Starý příběh
## Spirituál kvintet

1. Řek' `C`Mojžíš jednou lidu svému: Přišel čas,
dnes v noci tiše `Em`vytratí se `F`každý `G7`z nás!
`C`Má``C7``vá, `F`má``Fm``vá nám `C`všem svo`F`bodná `C`zem.

2. Já říkám rovnou každý ať s tím počítá
že naše cesta ke štěstí je trnitá.
Mává, mává nám všem svobodná zem.

> [: `C`A kdo se bojí vodou jít
ten podle tónu faraonů musí ``Dm``žít `G7`
`C`má``C7``vá, `F`má``Fm``vá nám `C`všem svo`F`bodná `C`zem. :]

3. Až první krůček bude jednou za námi
už nikdo nesmí zaváhat, dát na fámy.
Mává, mává nám všem svobodná zem.

4. Pak tenhle vandr všem potomkům ukáže
že šanci má jen ten, kdo má dost kuráže.
Mává, mává nám všem svobodná zem.

> [: A kdo se bojí… :]

5. Ten starý příběh z Bible vám tu vykládám
ať každý ví, že rozhodnout se musí sám.
Mává, mává nám všem svobodná zem.

> [: A kdo se bojí… :]
