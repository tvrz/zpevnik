# Dvě báby
## Spirituál kvintet

1. `D`Jak léta jdou, svět `G`pro mě ztrácí `D`glanc,
všichni se rvou a `A7`duši dávaj' všanc
a `D`za pár šestáků vás `G`prodaj', věřte `D`mi,
už víc nechci mít `G`domov `D`svůj `A7`na ze`D`mi!

> Čas žádá svý a mně se krátí dech,
když před kaplí tu zpívám na schodech
svou píseň vo nebi, kde bude blaze mi,
už víc nechci mít domov svůj na zemi!

2. Po jmění netoužím, jsme tu jen nakrátko,
i sláva je jak dým, jak prázdný pozlátko,
já koukám do voblak, až anděl kejvne mi,
už víc nechci mít domov svůj na zemi!

> Čas žádá svý…

3. Teď říkám „good-bye“ světskýmu veselí,
těm, co si užívaj', nechci lízt do zelí,
jsem hříšná nádoba, však spása kyne mi,
už víc nechci mít domov svůj na zemi!

> Čas žádá svý…

4. V určenej čas kytara dohraje,
zmlkne můj hlas na cestě do ráje,
vo tomhle špacíru noc co noc zdá se mi,
už víc nechci mít domov svůj na zemi!

> Čas žádá svý…
