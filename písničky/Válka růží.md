# Válka růží
## Spirituál kvintet

1. Už `Dm`rozplynul se `G`hustý dým, `Dm`derry down, hej, `A`down-a-down,
`Dm`nad ztichlým polem `G` válečným, derry `Dm`down,`A`
jen `F`ticho stojí `C`kolkolem`A7`    a `Dm`vítěz plení `B`vlastní `A`zem,
je válka `Dm`růží, `Gm`derry, derry, `A`derry down, a-`Dm`down.

1. Nečekej soucit od rváče, derry down, hej down-a-down,
kdo zabíjí ten nepláče, derry down,
na těle mrtvé krajiny se mečem píšou dějiny,
je válka růží, derry, derry, derry down, a-down.

1. Dva erby, dvojí korouhev, derry down, hej, down-a-down,
dva rody živí jeden hněv, derry down,
kdo změří, kam se nahnul trůn,
&nbsp;   zda k Yorkům nebo k Lancasterům,
je válka růží, derry, derry, derry down, a-down.

1. Dva erby, dvojí korouhev, derry down, hej, down-a-down,
však hlína pije jednu krev, derry down,
ať ten či druhý přežije, vždy nejvíce ztratí Anglie,
je válka růží, derry, derry, derry down, a-down.
